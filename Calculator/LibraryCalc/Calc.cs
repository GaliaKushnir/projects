﻿using System;
using System.Text.RegularExpressions;

namespace LibraryCalc
{
    public class Calc
    {
        public static void slice(string inputValue)
        {
            string firstNumber = "", secondNumber = "", equationOperator = "";
            ushort n = 1, end = 0;

            while (Regex.IsMatch(inputValue.Substring(0, n), @"^\d+$"))
            {
                firstNumber = inputValue.Substring(0, n);
                if (!(Regex.IsMatch(inputValue[n].ToString(), @"^\d+$")))
                {
                    equationOperator = inputValue[n].ToString();
                    break;
                }
                if (n > inputValue.Length)
                    break;
                n++;
            }
            end = (ushort)(inputValue.Length - (firstNumber.Length + 1));
            secondNumber = inputValue.Substring((n + 1), end);
            Calculate p = new Calculate(long.Parse(firstNumber), long.Parse(secondNumber), equationOperator);
        }
    }
 }

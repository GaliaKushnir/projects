﻿using System;

namespace LibraryCalc
{
    class Calculate
    {
        long firstNumber { get; set; }
        long secondNumber { get; set; }
        string result = "";
        string operation { get; set; }

        public Calculate(long firstNumber, long secondNumber, string equationOperator)
        {
            this.firstNumber = firstNumber;
            this.secondNumber = secondNumber;
            this.operation = equationOperator;

            switch (this.operation)
            {
                case "+":
                    result = (this.firstNumber + this.secondNumber).ToString();
                    break;
                case "-":
                    if (this.firstNumber < this.secondNumber)
                    {
                        result = ((-1) * (this.secondNumber - this.firstNumber)).ToString();
                        break;
                    }
                    result = (this.firstNumber - this.secondNumber).ToString();
                    break;
                case "/":
                    if (this.secondNumber == 0)
                    {
                        Console.WriteLine("\nCannot Divide by Zero!");
                        break;
                    }
                    double res = (double)this.firstNumber / this.secondNumber;

                    result = res.ToString();
                    break;
                case "*":
                    result = (this.firstNumber * this.secondNumber).ToString();
                    break;
            }

            Console.Write(" = {0}\n", result);
        }
    }
}
